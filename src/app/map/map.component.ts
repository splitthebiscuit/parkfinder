import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, NgZone, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MapsAPILoader, LatLngLiteral } from '@agm/core';
import { } from "googlemaps";

@Component({
  selector: 'map',
  templateUrl: './map.html'
})

export class MapComponent implements OnInit {
  constructor(private http: HttpClient,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { }

  address: string;
  query: string;
  parks: google.maps.places.PlaceResult[] = [];
  map: google.maps.Map;
  getNextPage: any = null;
  refresh: boolean = false;
  activeMarker: string;
  searching: boolean = false;
  isFirstLoad: boolean = true;
  latitude: number;
  longitude: number;
  zoom: number;

  @ViewChild('search', { static: false }) searchElementRef: ElementRef;
  @ViewChild('moreBtn', { static: false }) moreBtn: ElementRef;
  @ViewChild('parkList', { static: false }) parkList: ElementRef;

  @Output() hasCenter = new EventEmitter();

  needRefresh() {
    if (this.parks.length === 0 && !this.isFirstLoad) {
      this.findParks();
    }
    else
      this.refresh = true;
  }

  isOpen = (id) => this.activeMarker == id;

  //callback to search for more parks with same query
  moreParks() {
    this.moreBtn.nativeElement.disabled = true;
    if (this.getNextPage) this.getNextPage();
  }

  //query Google's API for nearby parks
  findParks() {
    this.refresh = false;
    this.searching = true;
    this.isFirstLoad = false;

    if (this.parkList) {
      this.parkList.nativeElement.scrollTop = 0;
    }
    //clear results if new location is used
    if (this.parks.length !== 0)
      this.parks = [];

    let parkService = new google.maps.places.PlacesService(this.map);

    parkService.nearbySearch({
      location: { lat: this.map.getCenter().lat(), lng: this.map.getCenter().lng() } as LatLngLiteral,
      radius: 50000,
      type: 'park',
      //rankBy: google.maps.places.RankBy.PROMINENCE,
      bounds: this.map.getBounds()
    }, (results, status, pagination) => {
      this.ngZone.run(() => {
        if (status !== google.maps.places.PlacesServiceStatus.OK)
        return;
        
        this.isFirstLoad = false;
        
        this.moreBtn.nativeElement.disabled = !pagination.hasNextPage;
        this.getNextPage = pagination.hasNextPage && function () {
          pagination.nextPage();
        };
        
        this.parks = [...this.parks, ...results];
        this.searching = false;
      })
    })
  }

  ngOnInit() {
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });

      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
          this.findParks();
        });
      });
    });
  }

  //pan map to selected park location
  setZoom(park: google.maps.places.PlaceResult) {
    this.activeMarker = park.id;
    this.map.panTo(park.geometry.location);
    this.map.setZoom(12);
  }

  mapReady(map: google.maps.Map) {
    this.map = map;
    this.findParks();
  }

  //set map to current location 
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }
}

interface ParkResults { data: ParkResult[] }
interface ParkResult { states: string, latLong?: string, coordinates: ParkCoordinates, fullName: string, description: string }
interface ParkCoordinates { lat: number, lng: number }

  // Nation Parks Service api
  // getParks() {
  //   this.http.get<ParkResults>('https://developer.nps.gov/api/v1/parks', {
  //     params: {
  //       api_key: '094WvLTfXrSTfm0Y8zmkl3TLNtTUzqW4oEx2X69W',
  //       q: this.query,
  //       limit: '50'
  //     }
  //   })
  //     .toPromise()
  //     .then(r => {
  //       this.parks = r.data.map<ParkResult>(v => ({
  //         states: v.states,
  //         coordinates: (v => ({ lat: +v[0], lng: +v[1] }))(v.latLong.split(',').map(c => c.split(':')[1])),
  //         // coordinates: {
  //         //   lat: v.latLong.split(',').map(c => c.split(':')[1])[0],
  //         //   lng: v.latLong.split(',').map(c => c.split(':')[1])[1]
  //         // }
  //         fullName: v.fullName,
  //         description: v.description
  //       }));
  //     })
  // }
