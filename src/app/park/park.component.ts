import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, NgZone, Input } from '@angular/core';
import { } from "googlemaps";

@Component({
    selector: '[park]',
    templateUrl: './park.html'
  })

export class ParkComponent{
  @Input()  park : google.maps.places.PlaceResult
}